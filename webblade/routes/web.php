<?php

use App\Http\Controllers\Cast;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Dashboard::class, 'index' ] );
Route::get('/table', function(){
    return view('page.table');
});
Route::get('/data-tables', function(){
    return view('page.data-table');
});

Route::get('/cast', [CastController::class, 'index' ] );
Route::get('/cast/create', [CastController::class, 'create' ] );
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast/{id}', [CastController::class, 'show']);
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{id}', [CastController::class, 'update']);
Route::delete('/cast/{id}',[CastController::class, 'destroy']);
