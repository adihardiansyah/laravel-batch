<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Cast extends Controller
{
    public function index()
    {
        return view('cast.add');
    }
    public function create()
    {
        return view('cast.add');
    }

    public function store(Request $request)
    {
    $request->validate([
        'nama' => 'required|unique:posts|max:255',
        'umur' => 'required',
        'bio' => 'required',
    ]);

    DB::table('cast')->insert([
        'nama' => $request->input('nama'),
        'umur' => $request->input('umur'),
        'bio' => $request->input('bio')
    ]);

    return redirect('\cast');
        // dd($request->all());
    }
}