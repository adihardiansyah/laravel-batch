@extends('dashboard')
@section('title')
Halaman Detail Cast
@endsection
@section('content')

<h1>Nama : {{$cast->nama}}</h1>
<h4>Usia Saat Ini : {{$cast->umur}}</h4>
<p>Bio : {{$cast->bio}}</p>

<a href="/cast" class="btn btn-sm btn-info">Kembali</a>
@endsection