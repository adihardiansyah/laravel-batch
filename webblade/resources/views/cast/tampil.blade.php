@extends('dashboard')
@section('title')
Halaman Dashboard Cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary">Tambah</a>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key=>$cs)
    <tr>
      <th scope="row">{{ $key + 1 }}</th>
      <td>{{ $cs->nama }}</td>
      <td>
          <form action="/cast/{{$cs->id}}" method="post">
            @csrf
            @method('delete')
            <a href="/cast/{{$cs -> id}}" class="btn btn-sm btn-info">Detail</a>
            <a href="/cast/{{$cs -> id}}/edit" class="btn btn-sm btn-warning">Edit</a>
            <input type="submit" value="Delete" class="btn btn-sm btn-danger">
        </form>
    </td>
      
    </tr>
    @empty
        <p>Tidak Ada Data Cast</p>
    @endforelse
    
    
  </tbody>
</table>



@endsection