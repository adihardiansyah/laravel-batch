@extends('dashboard')
@section('title')
Halaman Tambah Data Cast
@endsection
@section('content')

<form action="/cast" method="post">
    @csrf
    @error('title')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label >Nama</label>
    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" >
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label >Umur</label>
    <input type="text" class="form-control @error('umur') is-invalid @enderror"  name="umur">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label >Bio</label>
    <input type="text" class="form-control @error('bio') is-invalid @enderror"  name="bio">
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection