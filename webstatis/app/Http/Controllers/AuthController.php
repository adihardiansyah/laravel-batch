<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('regis');
    }

    public function get(Request $request)
    {
        $namaDepan = $request->input('fname');
        $namaBelakang = $request->input('lname');

        return view('wel',[
            'namaDepan' => $namaDepan,
            'namaBelakang' => $namaBelakang]
        );
    }
}
