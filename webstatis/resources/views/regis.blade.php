
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="\welcome" method="post">
    @csrf
      <label>First name :</label><br />
      <input type="text" name="fname"/><br />
      <label>Last name :</label><br />
      <input type="text" name="lname"/><br />
      <label>Gender</label><br /><br />
      <input type="radio" />Male <br />
      <input type="radio" />Female <br />
      <input type="radio" />Other <br /><br />
      <label>Nationality</label><br />
      <select name="" id="">
        <option value="">Indonesia</option>
        <option value="">Malaysia</option>
        <option value="">Japan</option>
        <option value="">Korea</option></select
      ><br /><br />
      <label>Language Spoken</label><br /><br />
      <input type="checkbox" name="" id="" />Bahasa Indonesia <br />
      <input type="checkbox" name="" id="" />English <br />
      <input type="checkbox" name="" id="" />Other <br /><br />
      <label>Bio</label> <br /><br />
      <textarea name="" id="" cols="30" rows="10"></textarea> <br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
